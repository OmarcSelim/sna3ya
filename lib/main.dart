import 'package:flutter/material.dart';
import 'package:sna3ya_user/splash.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        fontFamily: 'Cairo',
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreens(),
    ));
